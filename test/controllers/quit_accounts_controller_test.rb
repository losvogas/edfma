require 'test_helper'

class QuitAccountsControllerTest < ActionController::TestCase
  setup do
    @quit_account = quit_accounts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:quit_accounts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create quit_account" do
    assert_difference('QuitAccount.count') do
      post :create, quit_account: { done: @quit_account.done, email: @quit_account.email }
    end

    assert_redirected_to quit_account_path(assigns(:quit_account))
  end

  test "should show quit_account" do
    get :show, id: @quit_account
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @quit_account
    assert_response :success
  end

  test "should update quit_account" do
    patch :update, id: @quit_account, quit_account: { done: @quit_account.done, email: @quit_account.email }
    assert_redirected_to quit_account_path(assigns(:quit_account))
  end

  test "should destroy quit_account" do
    assert_difference('QuitAccount.count', -1) do
      delete :destroy, id: @quit_account
    end

    assert_redirected_to quit_accounts_path
  end
end
