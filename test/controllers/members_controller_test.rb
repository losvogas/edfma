require 'test_helper'

class MembersControllerTest < ActionController::TestCase
  test "creat congress member" do
    buyer = {email: "design.rupp@gmail.com", firstname: "Sebastian", lastname: "Rupp"}

    post create_congress_member(buyer), xhr: true

    assert_equal "done", @response.body
    
  end

end
