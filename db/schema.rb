# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170515090336) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string   "street"
    t.string   "plz"
    t.string   "city"
    t.string   "country"
    t.string   "tel"
    t.string   "fax"
    t.string   "web"
    t.string   "email"
    t.integer  "member_id"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "billing_address", default: false
    t.boolean  "primary",         default: false
    t.string   "office_name"
  end

  create_table "ahoy_events", force: :cascade do |t|
    t.integer  "visit_id"
    t.integer  "user_id"
    t.string   "name"
    t.json     "properties"
    t.datetime "time"
  end

  add_index "ahoy_events", ["name", "time"], name: "index_ahoy_events_on_name_and_time", using: :btree
  add_index "ahoy_events", ["user_id", "name"], name: "index_ahoy_events_on_user_id_and_name", using: :btree
  add_index "ahoy_events", ["visit_id", "name"], name: "index_ahoy_events_on_visit_id_and_name", using: :btree

  create_table "certificates", force: :cascade do |t|
    t.string   "name"
    t.integer  "course_id"
    t.integer  "instructor_id"
    t.integer  "organizer_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "special_id"
    t.integer  "member_id"
  end

  create_table "coupons", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "code"
    t.boolean  "code_used",  default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "name"
  end

  create_table "courses", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "begin"
    t.datetime "end"
    t.string   "link"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "city"
    t.integer  "instructor_id"
    t.integer  "organizer_id"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "course_type"
    t.string   "title"
  end

  create_table "designs", force: :cascade do |t|
    t.string   "name"
    t.string   "styling"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "member_files", force: :cascade do |t|
    t.string   "name"
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "document"
  end

  create_table "members", force: :cascade do |t|
    t.string   "title"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "grade"
    t.integer  "user_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "member_type",    default: 0
    t.string   "avatar"
    t.datetime "trial_ends"
    t.boolean  "certified",      default: false
    t.string   "organizer_name"
    t.string   "job"
    t.date     "birthday"
  end

  create_table "new_members", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "user_id"
    t.date     "next_payment"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "payment_customer_id"
  end

  create_table "people", force: :cascade do |t|
    t.string   "title"
    t.string   "firstname"
    t.date     "birthday"
    t.date     "deadthday"
    t.string   "lastname"
    t.string   "grade"
    t.integer  "quote_id"
    t.datetime "candle"
    t.string   "photo"
    t.integer  "design_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "place_id"
  end

  create_table "places", force: :cascade do |t|
    t.string   "name"
    t.integer  "staette_id"
    t.string   "description"
    t.string   "photo"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "quit_accounts", force: :cascade do |t|
    t.string   "email"
    t.boolean  "done"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "quotes", force: :cascade do |t|
    t.string   "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "scholar_categories", force: :cascade do |t|
    t.string   "custom_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "parent_id"
  end

  create_table "scholars", force: :cascade do |t|
    t.string   "title"
    t.string   "author"
    t.boolean  "public"
    t.string   "link"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "document"
    t.integer  "downloads",           default: 0
    t.integer  "scholar_category_id"
    t.string   "published"
    t.text     "content"
  end

  create_table "staettes", force: :cascade do |t|
    t.string   "name"
    t.string   "slug"
    t.string   "photo"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "url"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "visits", force: :cascade do |t|
    t.string   "visit_token"
    t.string   "visitor_token"
    t.string   "ip"
    t.text     "user_agent"
    t.text     "referrer"
    t.text     "landing_page"
    t.integer  "user_id"
    t.string   "referring_domain"
    t.string   "search_keyword"
    t.string   "browser"
    t.string   "os"
    t.string   "device_type"
    t.integer  "screen_height"
    t.integer  "screen_width"
    t.string   "country"
    t.string   "region"
    t.string   "city"
    t.string   "postal_code"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.string   "utm_source"
    t.string   "utm_medium"
    t.string   "utm_term"
    t.string   "utm_content"
    t.string   "utm_campaign"
    t.datetime "started_at"
  end

  add_index "visits", ["user_id"], name: "index_visits_on_user_id", using: :btree
  add_index "visits", ["visit_token"], name: "index_visits_on_visit_token", unique: true, using: :btree

end
