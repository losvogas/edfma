class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :street
      t.string :plz
      t.string :city
      t.string :country
      t.string :tel
      t.string :fax
      t.string :web
      t.string :email
      t.boolean :primary
      t.integer :member_id
      t.float :latitude
      t.float :longitude

      t.timestamps null: false
    end
  end
end
