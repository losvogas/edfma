class ChangeUserIdInPaymentsToInteger < ActiveRecord::Migration
  def change
  	change_column :payments, :user_id, 'integer USING CAST(user_id AS integer)'
  end
end

