class AddcertifiedtoMembers < ActiveRecord::Migration
  def change
  	add_column :members, :certified, :boolean, :default => false
  	add_column :members, :organizer_name, :string
  end
end
