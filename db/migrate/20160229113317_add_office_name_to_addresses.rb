class AddOfficeNameToAddresses < ActiveRecord::Migration
  def change
    add_column :addresses, :office_name, :string
  end
end
