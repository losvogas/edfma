class AddDocumentToScholar < ActiveRecord::Migration
  def change
    add_column :scholars, :document, :string
  end
end
