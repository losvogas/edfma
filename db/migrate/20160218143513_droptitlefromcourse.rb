class Droptitlefromcourse < ActiveRecord::Migration
  def change
  	remove_column :courses, :title
  	add_column :members, :job, :string
  	add_column :members, :birthday, :date
  end
end
