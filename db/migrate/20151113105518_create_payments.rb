class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :user_id
      t.string :paymill_customer_id
      t.date :next_payment

      t.timestamps null: false
    end
  end
end
