class CreateScholars < ActiveRecord::Migration
  def change
    create_table :scholars do |t|
      t.string :title
      t.string :author
      t.date :published
      t.string :categories
      t.boolean :public
      t.string :link

      t.timestamps null: false
    end
  end
end
