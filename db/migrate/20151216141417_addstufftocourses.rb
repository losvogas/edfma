class Addstufftocourses < ActiveRecord::Migration
  def change
  	add_column :courses, :latitude, :float
  	add_column :courses, :longitude, :float
  	add_column :courses, :course_type, :string
  end
end
