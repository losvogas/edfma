class ChangeDownloadsInScholar < ActiveRecord::Migration
  def change
  	change_column :scholars, :downloads, :integer, :default => 0
  end
end
