class CreateQuitAccounts < ActiveRecord::Migration
  def change
    create_table :quit_accounts do |t|
      t.string :email
      t.boolean :done

      t.timestamps null: false
    end
  end
end
