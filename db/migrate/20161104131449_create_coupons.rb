class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.integer :user_id
      t.string :code
      t.boolean :code_used

      t.timestamps null: false
    end
  end
end
