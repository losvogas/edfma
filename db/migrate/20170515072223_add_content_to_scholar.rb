class AddContentToScholar < ActiveRecord::Migration
  def change
    add_column :scholars, :content, :text
  end
end
