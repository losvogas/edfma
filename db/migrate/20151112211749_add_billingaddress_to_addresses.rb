class AddBillingaddressToAddresses < ActiveRecord::Migration
  def change
    add_column :addresses, :billing_address, :boolean, default: false
    remove_column :addresses, :primary
    add_column :addresses, :primary, :boolean, default: false
  end
end
