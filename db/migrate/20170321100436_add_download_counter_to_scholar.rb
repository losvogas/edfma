class AddDownloadCounterToScholar < ActiveRecord::Migration
  def change
    add_column :scholars, :downloads, :integer
  end
end
