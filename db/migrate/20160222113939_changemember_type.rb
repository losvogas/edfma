class ChangememberType < ActiveRecord::Migration
  def change
  	change_column :members, :member_type, :integer, default: 0
  end
end
