class Changecolumnsincourses < ActiveRecord::Migration
  def change
  	remove_column :courses, :name
  	add_column :courses, :title, :string, :null => false
  	remove_column :courses, :city
  	add_column :courses, :city, :string, :null => false
  	add_column :courses, :instructor_id, :integer, :null => false
  	add_column :courses, :organizer_id, :integer, :null => false

  end
end
