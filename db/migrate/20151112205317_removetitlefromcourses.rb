class Removetitlefromcourses < ActiveRecord::Migration
  def change
  	remove_column :courses, :title
  	add_column :courses, :title, :string, empty: false
  end
end
