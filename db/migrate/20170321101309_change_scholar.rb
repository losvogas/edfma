class ChangeScholar < ActiveRecord::Migration
  def change
  	remove_column :scholars, :categories
  	add_column :scholars, :scholar_category_id, :integer
  end
end
