class AddDocumentToMemberfiles < ActiveRecord::Migration
  def change
    add_column :member_files, :document, :string
  end
end
