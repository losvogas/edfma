class CreateMemberFiles < ActiveRecord::Migration
  def change
    create_table :member_files do |t|
      t.string :name
      t.string :category

      t.timestamps null: false
    end
  end
end
