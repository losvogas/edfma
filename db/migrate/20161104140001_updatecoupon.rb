class Updatecoupon < ActiveRecord::Migration
  def change
  	change_column :coupons, :code_used, :boolean, default: false
  end
end
