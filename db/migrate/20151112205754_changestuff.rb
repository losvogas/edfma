class Changestuff < ActiveRecord::Migration
  def change
  	  	remove_column :courses, :city
  	  	add_column :courses, :city, :string, empty: false
  	  	remove_column :courses, :instructor_id
  	  	add_column :courses, :instructor_id, :integer, empty: false
  	  	remove_column :courses, :organizer_id
  	  	add_column :courses, :organizer_id, :integer, empty: false

  end
end
