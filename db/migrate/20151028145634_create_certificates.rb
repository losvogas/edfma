class CreateCertificates < ActiveRecord::Migration
  def change
    create_table :certificates do |t|
      t.string :name, nil: false
      t.integer :course_id, nil: false
      t.integer :instructor_id, nil: false
      t.integer :organizer_id, nil: false

      t.timestamps null: false
    end
  end
end
