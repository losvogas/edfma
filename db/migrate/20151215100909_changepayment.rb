class Changepayment < ActiveRecord::Migration
  def change
  	remove_column :payments, :paymill_customer_id
  	add_column :payments, :payment_customer_id, :string
  end
end
