class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.integer :user_id
      t.integer :name
      t.integer :city
      t.datetime :begin
      t.datetime :end
      t.string :link

      t.timestamps null: false
    end
  end
end
