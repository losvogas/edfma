class AddSpecialIdToCertificates < ActiveRecord::Migration
  def change
    add_column :certificates, :special_id, :string, null: :false
  end
end
