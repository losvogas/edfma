class AddMemberIdToCertificate < ActiveRecord::Migration
  def change
    add_column :certificates, :member_id, :integer
  end
end
