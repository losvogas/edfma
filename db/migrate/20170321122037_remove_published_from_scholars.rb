class RemovePublishedFromScholars < ActiveRecord::Migration
  def change
  	remove_column :scholars, :published
  	add_column :scholars, :published, :string
  end
end
