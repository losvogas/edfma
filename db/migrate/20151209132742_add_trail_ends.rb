class AddTrailEnds < ActiveRecord::Migration
  def change
  	add_column :members, :trial_ends, :datetime, :empty => false
  end
end
