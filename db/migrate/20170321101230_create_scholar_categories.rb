class CreateScholarCategories < ActiveRecord::Migration
  def change
    create_table :scholar_categories do |t|
      t.string :custom_id
      t.string :name

      t.timestamps null: false
    end
  end
end
