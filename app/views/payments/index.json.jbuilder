json.array!(@payments) do |payment|
  json.extract! payment, :id, :user_id, :paymill_customer_id, :next_payment
  json.url payment_url(payment, format: :json)
end
