json.array!(@member_files) do |member_file|
  json.extract! member_file, :id, :name, :category
  json.url member_file_url(member_file, format: :json)
end
