json.array!(@quit_accounts) do |quit_account|
  json.extract! quit_account, :id, :email, :done
  json.url quit_account_url(quit_account, format: :json)
end
