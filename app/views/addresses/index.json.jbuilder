json.array!(@addresses) do |address|
  json.extract! address, :id, :street, :plz, :city, :country, :tel, :fax, :web, :email, :primary, :member_id, :latitude, :longitude
  json.url address_url(address, format: :json)
end
