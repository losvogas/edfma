json.array!(@scholars) do |scholar|
  json.extract! scholar, :id, :title, :author, :published, :categories, :public, :link
  json.url scholar_url(scholar, format: :json)
end
