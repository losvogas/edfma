class UserMailer < ApplicationMailer
	default from: "Office EFDMA <office@fdm-europe.com>"

	def confirmation_payment(user)
		@user = user
		@link = root_url + "payments"
		mail(to: @user.email, subject: "EFDMA Membership: Confirmation of your payment")

	end

	def welcome_instructor(user)
		@user = user
		@link = root_url
		mail(to: @user.email, subject: "EFDMA Instructor: New members area")
	end

	def welcome_member(user)
		@user = user
		@link = root_url
		mail(to: @user.email, bcc: "office@fdm-europe.com", subject: "EFDMA Members: New members area")
	end

	def resend_member_email(user)
		@user = user
		@pass = Devise.friendly_token.first(8)
		@user.update(password: @pass)
		mail(to: @user.email, bcc: "office@fdm-europe.com", subject: "EFDMA Login Information")
	end

	def efdma_notification_new_member(user)
		@user = user
		unless @user.has_role?(:organizer)
			mail(to: "office@fdm-europe.com", subject: "New member: #{@user.member.full_name}")
	    end
	end

	def member_paid(user, payment_method)
		@user = user
		@payment_method = payment_method

		mail(to: "office@fdm-europe.com", subject: "Member (#{@user.member.full_name}) has paid")
	end

	def failed_subscriptionpayment(user)
		@user = User.find(user)
		mail(to: "office@fdm-europe.com", subject: "EFDMA: failed payment")
	end

	def new_payment_method(user)
		@user = User.find(user)
		mail(to: "sebastian@sebastian-rupp.com", subject: "EFDMA: new payment")
	end

	



end
