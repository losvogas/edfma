class DeleteFromMailingListJob < ActiveJob::Base

	Gibbon::Request.api_key = "b77edbd68a1fa8b6c0925eb1aee7b7c6-us12"
	Gibbon::Request.timeout = 15
	queue_as :default
	

  def perform(u)
    gb = Gibbon::Request.new
    
    hashed_email = Digest::MD5.hexdigest(u.email)
    begin
    result = gb.lists("f8ae4dcc0d").members(hashed_email).delete
    Rails.logger.debug "DELETE USER FROM MAILCHIMP: #{result}"
    rescue
      Rails.logger.debug "not found on Mailchimp"
    end 
  end
end
