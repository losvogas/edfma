class UpdateUserInMailingListJob < ActiveJob::Base
  
  	Gibbon::Request.api_key = "b77edbd68a1fa8b6c0925eb1aee7b7c6-us12"
	Gibbon::Request.timeout = 15
	
	queue_as :default

  def perform(user)
  	hashed_email = Digest::MD5.hexdigest(user.email_was)

    gb = Gibbon::Request.new
    gb.lists("f8ae4dcc0d").members(hashed_email).upsert(body: {email_address: user.email, status: "subscribed"})
  end
end
