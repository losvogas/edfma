class AddressesController < ApplicationController
  
  before_filter :authenticate_user!, except: [:show, :index, :search]
  before_action :set_address, only: [:show, :edit, :update, :destroy]


  # GET /addresses
  # GET /addresses.json
  def index
    @member = Member.find(params[:member_id])
    @addresses = @member.addresses
  end




  # GET /addresses/1
  # GET /addresses/1.json
  def show
    @member = @address.member
  end

  def search
    #@addresses = Address.near(params[:search], 100).to_a
   @addresses = Address.joins(:member).where(primary: true).where("member_type > 0 OR trial_ends > ?", Date.today - 1.day).where("member_type < 3").near(params[:search], 100).to_a 


    i = 0
    while i <= @addresses.length 
    

      
      x = i + 1

      unless @addresses[x].nil?


        current_address = Address.find(@addresses[i].id)
        unless current_address.member.user.payments.last.nil?
        Rails.logger.debug "#{current_address.member.user.payments.last.next_payment.to_date}"
        end 
        payments = current_address.member.user.payments
        
        if current_address.member.user.has_role?("organizer")
          @addresses.delete_at(i)
          
        elsif payments.last.nil? && current_address.member.member_type > 0 
            @addresses.delete_at(i)  
        elsif current_address.member.member_type == 0 && current_address.member.trial_ends.to_date < Date.today
            @addresses.delete_at(i)  

        elsif @addresses[i].primary == false
            @addresses.delete_at(i)

        elsif @addresses[i].member_id == @addresses[x].member_id
            @addresses.delete_at(x)

        else

          if payments.count > 0 
            if payments.last.next_payment.to_date < Date.today && payments.last.payment_customer_id.nil?
                Rails.logger.debug "DELETED MOTHERFUCKER"
                p current_address.member.full_name
                @addresses.delete_at(i)
              
            end
          end

          if @addresses[i].primary == true
            
          else
            @addresses.delete_at(i)
          end
        end
      end
      i = x
    end



          

    
    respond_to do |format|
      format.html
      format.json { render json: @addresses, include: [:member], content_type: "application/javascript", callback: params["callback"] }
    end
    

  end

  # GET /addresses/new
  def new
    @address = Address.new
  end

  # GET /addresses/1/edit
  def edit
    @address = current_user.addresses.find(params[:id])
    
  end

  # POST /addresses
  # POST /addresses.json
  def create
    @address = Address.new(address_params)
    @address.member_id = current_user.member.id

    if Member.find(@address.member_id).addresses.count == 0
      @address.primary = true
      @address.billing_address = true
    end
    
    
    respond_to do |format|
      if @address.save
        format.html { redirect_to edit_member_path(current_user.member), notice: 'Address was successfully created.' }
        format.json { render :show, status: :created, location: @address }
      else
        format.html { render :new }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /addresses/1
  # PATCH/PUT /addresses/1.json
  def update
    @member = @address.member
    respond_to do |format|
      if @address.update(address_params)

        if @member.addresses.count == 1
          @member.addresses.first.update(primary: true, billing_address: true)
        end


        format.html { redirect_to edit_member_path(@member), notice: 'Address was successfully updated.' }
        format.json { render :show, status: :ok, location: @address }
      else
        format.html { render :edit }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /addresses/1
  # DELETE /addresses/1.json
  def destroy
    member = @address.member
    @address.destroy
    
    if member.addresses.count == 1
      member.addresses.first.update(primary: true, billing_address: true)
    end


    respond_to do |format|
      format.html { redirect_to edit_member_path(@address.member), notice: 'Address was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address
      @address = Address.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def address_params
      params.require(:address).permit(:office_name, :street, :plz, :city, :country, :tel, :fax, :web, :email, :primary, :billing_address, :member_id, :latitude, :longitude)
    end
end
