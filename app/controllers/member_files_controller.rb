class MemberFilesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_member_file, only: [:show, :edit, :update, :destroy]

  # GET /member_files
  # GET /member_files.json
  def index
    @member_files = MemberFile.all
    if Scholar.all.where(public: true).count > 0
      @scholars = Scholar.where(public: true)
    end
  end

  # GET /member_files/1
  # GET /member_files/1.json
  def show
  end

  # GET /member_files/new
  def new
    unless current_user.has_role?("admin")
      redirect_to member_files_path, notice: "You are not allowed to upload files"
    end
    @member_file = MemberFile.new
  end

  # GET /member_files/1/edit
  def edit
    unless current_user.has_role?("admin")
      redirect_to member_files_path, notice: "You are not allowed to edit files"
    end
  end

  # POST /member_files
  # POST /member_files.json
  def create
    @member_file = MemberFile.new(member_file_params)

    respond_to do |format|
      if @member_file.save
        format.html { redirect_to member_files_path, notice: 'File was successfully uploaded.' }
        format.json { render :show, status: :created, location: @member_file }
      else
        format.html { render :new }
        format.json { render json: @member_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /member_files/1
  # PATCH/PUT /member_files/1.json
  def update
    respond_to do |format|
      if @member_file.update(member_file_params)
        format.html { redirect_to @member_file, notice: 'File was successfully updated.' }
        format.json { render :show, status: :ok, location: @member_file }
      else
        format.html { render :edit }
        format.json { render json: @member_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /member_files/1
  # DELETE /member_files/1.json
  def destroy
    unless current_user.has_role?("admin")
      redirect_to member_files_path, notice: "You are not allowed to delete files"
    end
    @member_file.destroy
    respond_to do |format|
      format.html { redirect_to member_files_url, notice: 'File was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member_file
      @member_file = MemberFile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def member_file_params
      params.require(:member_file).permit(:name, :category, :document)
    end
end
