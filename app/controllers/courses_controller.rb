class CoursesController < ApplicationController
  
  before_filter :authenticate_user!, except: [:show, :index]
  #before_filter :not_paid
  before_action :set_course, only: [:show, :edit, :update, :destroy]
  

  # GET /courses
  # GET /courses.json
  def index
    @courses = Course.where("begin >= ?", Time.now - 1.day).order(:begin)

    respond_to do |format|
      format.html
      format.json { render json: @courses, include: [:instructor, :organizer], content_type: "application/javascript", callback: params["callback"] }
    end
  end

  # GET /courses/1
  # GET /courses/1.json
  def show
  end

  # GET /courses/new
  def new
    ability = Ability.new(current_user)
    if ability.can? :manage, Course
      @course = Course.new
      @instructors = User.with_role(:instructor).order("lastname")
      @organizers = User.with_role(:organizer)
    else
      redirect_to courses_path, notice: "You are not allowed to create courses."
    end
  end

  # GET /courses/1/edit
  def edit
    ability = Ability.new(current_user)
    if ability.can? :write, Course
      
    else
      redirect_to courses_path, notice: "You are not allowed to edit courses."
    end
  end

  # POST /courses
  # POST /courses.json
  def create

    @course = Course.new(course_params)
    @course.user_id = current_user.id

    respond_to do |format|
      if @course.save
        format.html { redirect_to edit_overview_path, notice: 'Course was successfully created.' }
        format.json { render :show, status: :created, location: @course }
      else
        format.html { render :new }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    ability = Ability.new(current_user)
    if ability.can? :write, Course
      respond_to do |format|
      if @course.update(course_params)
        format.html { redirect_to edit_overview_path, notice: 'Course was successfully updated.' }
        format.json { render :show, status: :ok, location: @course }
      else
        format.html { render :edit }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
    else
      redirect_to courses_path, notice: "You are not allowed to update courses."
    end


    
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    @course.destroy
    respond_to do |format|
      format.html { redirect_to courses_url, notice: 'Course was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def edit_overview
    @courses = Course.where("user_id = #{current_user.id} OR instructor_id = #{current_user.id} OR organizer_id = #{current_user.id}").where("begin > ?", Time.now).order(:end)
  end

  private
    def not_paid
      unless current_user.member.member_type == 4 # Members with member_type 4 are administrative Admins
        if current_user.payment.nil? && current_user.member.member_type > 0
        redirect_to new_payment_path
      elsif current_user.member.member_type == 0 && current_user.member.trial_ends < Time.now
        redirect_to new_payment_path
      end
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:user_id, :title, :city, :begin, :end, :link, :instructor_id, :organizer_id, :course_type, :longitude, :latitude)
    end
end
