class CouponController < ApplicationController

  respond_to :json



  def check
  	coupon = Coupon.find_by(code: params[:code])

  	if coupon.nil?
  		@response = "wrong"
  	elsif coupon.code_used == false
  		@response = "good"
  	else 
  		@response = "used"
  	end

  	respond_with @response
  		
  end

  def used_now
    unless params[:code] == "s-IQ05UrENI5Cy9E09XRQw" || params[:code] == "TNmG-JVSgIiwnzNfqulyBQ" 
    	@coupon = Coupon.find_by(code: params[:code])

    	@coupon.code_used = true

    	if @coupon.save
    		respond_with @coupon
    	else
    		respond_with @coupon.errors
    	end

      

    else 
      respond_with @coupon
    end
  end

  


end
