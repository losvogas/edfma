  class AdminController < ApplicationController

  before_filter :authenticate_user!, except: [:braintree_webhooks]
  skip_before_action :verify_authenticity_token, if: :braintree_webhooks

  respond_to :html, :json

  def braintree_webhooks

    @data = {data: "lala"}
    
    if params["bt_signature"]
      webhook_notification = Braintree::WebhookNotification.parse(
        request.params["bt_signature"],
        request.params["bt_payload"]
      )

      customer_id = webhook_notification.subscription.transactions.first.customer_details.id

      user = Payment.where(payment_customer_id: customer_id).first.user

      p = UserMailer.failed_subscriptionpayment(user.id).deliver_now
      Rails.logger.debug "#{p}"

      
    end

    

  end

  def stats
  end

  def look_up
    @members = Member.order("created_at DESC").take(10)
    @names = []

    Member.all.each do  |member|
      @names.push(member.full_name)
    end

  end

  def list_searched_members
    person_name = params[:person_name]
    @members = Member.search_by_full_name(person_name)

    if @members.count == 0
      if params[:person_name].include?("@")
        member = User.where(email: person_name).first.member
        @members << member
      else
        member = Member.search_by_braintree(person_name)
        @members << member
      end
    end
  

    
    

    respond_to do |format|
      format.js
    end
  end

  def edit_user
  	@member = Member.find(params[:user_id])
    @user = @member.user
    @certificates = @user.certificates
    @coupons = @user.coupon

    if @user.payments.last
    unless @user.payments.last.payment_customer_id.nil? 
    	@payments = Braintree::Transaction.search do |search|
        search.customer_id.is @user.payments.last.payment_customer_id
      end
    end
    end

  end

  def change_user

  	user = User.find(params[:user_id])
    member = user.member
    user.update(user_params)
    member.update(member_params)
    



    redirect_to admin_look_up_path, notice: "User changed"
  end

  def new_user
    @user = User.new
    @user.build_member
  end

  def create_user
    @user = User.new(user_params)
    @user.password = Devise.friendly_token.first(8)
    
    if @user.save
     redirect_to admin_edit_user_path(@user.member.id), notice: "New Member created"
    else 
      redirect_to admin_new_user_path(@user)
    end
  end


  def delete_user
    @user = User.find(params[:id])
    DeleteFromMailingListJob.perform_later(@user)

    

    if @user.delete
      unless @user.member.addresses.nil?
        @user.member.addresses.delete_all
      end
    
      unless @user.payments.first.nil?

        unless @user.payments.last.payment_customer_id.nil?
          b = Braintree::Customer.find(payment.payment_customer_id)
          s = b.payment_methods.map(&:subscriptions).flatten.last

          result = Braintree::Subscription.cancel(s.id)
        end

        @user.payments.delete_all
      end
      @user.member.delete
      redirect_to admin_look_up_path, notice: "Member was deleted"
    else
      redirect_to admin_look_up_path, notice: "Something has gone wrong."
    end
  end

  def index_organizer
    @organizers = User.with_role("organizer")
  end

  def new_organizer
    @organizer = User.new
    @organizer.build_member
  end

  def edit_organizer
    @organizer = User.find(params[:id])
  end

  def create_organizer
    organizer = User.new(user_params)
    organizer.email = Devise.friendly_token.first(8) + "@gmail.com"
    organizer.password = Devise.friendly_token.first(8)
    if organizer.save
      organizer.add_role("organizer")
      redirect_to admin_edit_organizer_path(organizer)
    end
  end

  def update_organizer
    @organizer = User.find(params[:id])
    @organizer.update(user_params)
    redirect_to admin_organizers_path, notice: "Updated"
  end

  def delete_organizer
    @organizer = User.find(params[:id])
    if @organizer.delete
      redirect_to admin_organizers_path, notice: "Organizer has been deleted"
    end
  end




  def resend_member_email
    @user = User.find params[:user_id]
    UserMailer.resend_member_email(@user).deliver
    redirect_to admin_look_up_path, notice: "Email with login data has been sent"
  end


  def new_address
    @user = User.find(params[:user_id])
    @member = @user.member
    @address = Address.new
    
  end

  def create_address
    @user = User.find(params[:user_id])
    @member = @user.member
    @address = Address.new(address_params)
    @address.member_id = @user.member.id
    if @address.save
      redirect_to admin_edit_user_path(@user.member.id), notice: "Address was created."
    else
      render :new_address
    end
  end

  def destroy_address
    @address = Address.find params[:id]
    member = @address.member
    @address.destroy
    
    if member.addresses.count == 1
      member.addresses.first.update(primary: true, billing_address: true)
    end


    respond_to do |format|
      format.html { redirect_to admin_edit_user_path(member), notice: 'Address was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def edit_address
    @address = Address.find(params[:id])
    @member = Member.find(@address.member_id)
    
  end

  def update_address
    @address = Address.find(params[:id])
    @member = Member.find(@address.member.id)
    respond_to do |format|
      if @address.update(address_params)

        if @member.addresses.count == 1
          @member.addresses.first.update(primary: true, billing_address: true)
        end


        format.html { redirect_to admin_edit_user_path(@member), notice: 'Address was successfully updated.' }
        format.json { render :show, status: :ok, location: @address }
      else
        format.html { render :edit }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  def has_paid
    @payments = Payment.all.order("created_at DESC")
    @users = User.all.joins(:member).where("members.member_type > 0").joins("INNER JOIN payments ON payments.user_id = users.id").order("payments.created_at DESC")
    #Post.joins("INNER JOIN memberships ON memberships.group_id = posts.group_id").where(:memberships => {:user_id => current_user.id})


  end

  def send_payment_reminder
    @user = User.find(params[:id])
    UserMailer.payment_reminder(@user).deliver
    redirect_to admin_has_paid_path, notice: "Payment reminder has been sent"
  end

  def who_became_member
    @new_members = NewMember.all.order("created_at DESC")
  end

  def new_payment_for_member
    @user = User.find(params[:user_id])
  end

  def create_payment_for_member
    @user = User.find(params[:user_id])
    @payment = Payment.new(user_id: params[:user_id], next_payment: params[:next_payment])


    
    if @payment.save

      if @payment.user.member.member_type == 0
        @payment.user.member.update(member_type: 1)
      end

      redirect_to admin_edit_user_path(@payment.user), notice: "Payment created for #{@payment.user.member.full_name}"
    else
      render :new_payment_for_member, notice: "Please check your input data or payment already exists."
    end

    
      
  end

  def show_invoice
    @user = User.find(params[:user_id])
    @show = true
    @member = @user.member
    @payment = @user.payments.last


    if @payment.nil?
      redirect_to admin_edit_user_path(@member.id), notice: "Member hasn't paid yet"
    end

    if @member.addresses.where(billing_address: true).first.nil?
      redirect_to admin_edit_user_path(@member.id), notice: "Member has NO billing address"
    end

    if @member.addresses.where(billing_address: true).first.nil?
      @show = false
    else


      if params[:id].nil?

        @billing_address = @member.addresses.where(billing_address: true).first
        Rails.logger.debug "#{@billing_address}"

        pdf = Prawn::Document.new page_size: "A4"
        image = Rails.root.join('app','assets','images','Logo-4C.jpg').to_s
        pdf.image image, :width => 250, position: :right
        pdf.pad_top (40) {pdf.text "from\nEFDMA\nLerchenfelder Straße 66-68/1/52\n1080 Vienna\nAustria", align: :left, size: 12 }
            
        text_block  = "to\n" + @member.full_name
        text_block += "\n"
        text_block += @billing_address.street + "\n" + @billing_address.plz + " " + @billing_address.city + "\n" +  @billing_address.country+ "\n"

        pdf.pad_top (40) {pdf.text text_block, align: :left, size: 12 }
        pdf.pad_top (40) {pdf.text "Vienna, #{@payment.created_at.strftime("%Y-%m-%d")}", align: :right, size: 12 }
        pdf.pad_top (20) {pdf.text "Dear #{@member.title} #{@member.firstname} #{@member.lastname} #{@member.grade},\n this is a confirmation of your payment for the EFDMA membership fee for the next year.", align: :left, size: 12}
        pdf.pad_top (20) {pdf.text "We successfully received your payment of € 140,-.", align: :left, size: 12, style: :bold}
        pdf.pad_top (20) {pdf.text "Thank you for being a part of EFDMA.", align: :left, size: 12}
        pdf.pad_top (40) {pdf.text "Yours sincerely,\n\n Markus Nagel MSc\nTreasurer EFDMA", align: :left, size: 12}

        pdf.bounding_box([0,50], width: 150, height: 50) do
          pdf.text "EFDMA\nLerchenfelder Straße 66-68/1/52\nA-1080 Vienna", size: 10
        end
        pdf.bounding_box([190,50], width: 150, height: 50) do
          
          pdf.text "wwww.fdm-europe.com\noffice@fdm-europe.com\nZVR 898629488", size: 10, align: :center
        end
        pdf.bounding_box([370,50], width: 150, height: 50) do
          
          pdf.text "IBAN: AT401200050030001405\nBIC:BKAUATWW\nBank Austria - Creditanstalt", size: 10, align: :right
        end
        send_data pdf.render, filename: "invoice_EFDMA_#{@user.id}.pdf", type: "application/pdf"
      
      else

        @transaction = Braintree::Transaction.find(params[:id])
        Rails.logger.debug "#{@transaction.inspect}"
        @billing_address = @member.addresses.where(billing_address: true).first
        Rails.logger.debug "#{@billing_address.inspect}"

        pdf = Prawn::Document.new page_size: "A4"
        image = Rails.root.join('app','assets','images','Logo-4C.jpg').to_s
        pdf.image image, :width => 250, position: :right
        pdf.pad_top (40) {pdf.text "from\nEFDMA\nLerchenfelder Straße 66-68/1/52\n1080 Vienna\nAustria", align: :left, size: 12 }
            
        text_block  = "to\n" + @member.full_name
        text_block += "\n"
        text_block += @billing_address.street + "\n" + @billing_address.plz + " " + @billing_address.city + "\n" +  @billing_address.country+ "\n"

        pdf.pad_top (40) {pdf.text text_block, align: :left, size: 12 }
        pdf.pad_top (40) {pdf.text "Vienna, #{@transaction.created_at.strftime("%Y-%m-%d")}", align: :right, size: 12 }
        pdf.pad_top (10) {pdf.text "Payment confirmation (ID: #{@transaction.id})", align: :left, size: 12, style: :bold }
        pdf.pad_top (20) {pdf.text "Dear #{@member.title} #{@member.firstname} #{@member.lastname} #{@member.grade},\n this is a confirmation of your payment for the EFDMA membership fee for the next year, starting today.", align: :left, size: 12}
        pdf.pad_top (20) {pdf.text "We successfully received your payment of € #{@transaction.amount}.", align: :left, size: 12, style: :bold}
        pdf.pad_top (20) {pdf.text "Thank you for being a part of EFDMA.", align: :left, size: 12}
        pdf.pad_top (40) {pdf.text "Yours sincerely,\n\n Markus Nagel MSc\nTreasurer EFDMA", align: :left, size: 12}

        pdf.bounding_box([0,50], width: 150, height: 50) do
          pdf.text "EFDMA\nLerchenfelder Straße 66-68/1/52\nA-1080 Vienna", size: 10
        end
        pdf.bounding_box([190,50], width: 150, height: 50) do
          pdf.text "www.fdm-europe.com\noffice@fdm-europe.com\nZVR 898629488", size: 10, align: :center
        end
        pdf.bounding_box([370,50], width: 150, height: 50) do
          pdf.text "IBAN: AT401200050030001405\nBIC:BKAUATWW\nBank Austria - Creditanstalt", size: 10, align: :right
        end

        send_data pdf.render, filename: "invoice_EFDMA_#{@user.id}.pdf", type: "application/pdf"
      end
    end
  end 




  private

  def user_params 
    params.require(:user).permit(:email, :password, member_attributes: [:id, :title, :firstname, :lastname, :grade, :avatar, :job, :member_type, :certified, :organizer_name, :trial_ends])
  end

  def member_params
    params.require(:user).require(:member_attributes).permit(:title, :firstname, :lastname, :grade, :avatar, :job, :member_type, :certified)
  
  end

  def address_params
    params.require(:address).permit(:street, :city, :plz, :billing_address, :primary, :office_name, :country, :tel, :fax, :web, :email)
  end



end
