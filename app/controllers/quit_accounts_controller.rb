class QuitAccountsController < ApplicationController
  before_action :set_quit_account, only: [:show, :edit, :update, :destroy]

  # GET /quit_accounts
  # GET /quit_accounts.json
  def index
    @quit_accounts = QuitAccount.all
  end

  # GET /quit_accounts/1
  # GET /quit_accounts/1.json
  def show
  end

  # GET /quit_accounts/new
  def new
    @quit_account = QuitAccount.new
  end

  # GET /quit_accounts/1/edit
  def edit
  end


  # POST /quit_accounts
  # POST /quit_accounts.json
  def create
    emails_list = params[:mails_list]
    @list_array = emails_list.split("\r\n")

    for email in @list_array
      QuitAccount.create(email: email, done: false)
    end


    redirect_to action: :index
  end

  # PATCH/PUT /quit_accounts/1
  # PATCH/PUT /quit_accounts/1.json
  def update
    respond_to do |format|
      if @quit_account.update(quit_account_params)
        format.html { redirect_to @quit_account, notice: 'Quit account was successfully updated.' }
        format.json { render :show, status: :ok, location: @quit_account }
      else
        format.html { render :edit }
        format.json { render json: @quit_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quit_accounts/1
  # DELETE /quit_accounts/1.json
  def destroy
    @quit_account.destroy
    respond_to do |format|
      format.html { redirect_to quit_accounts_url, notice: 'Quit account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quit_account
      @quit_account = QuitAccount.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quit_account_params
      params.require(:quit_account).permit(:email, :done)
    end
end
