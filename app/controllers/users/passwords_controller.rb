class Users::PasswordsController < Devise::PasswordsController
  before_filter :authenticate_user!
  before_filter :configure_permitted_parameters
  layout "empty"
  # GET /resource/password/new
  def new
     super
   end

  # POST /resource/password
  # def create
  #   super
  # end

  # GET /resource/password/edit?reset_password_token=abcdef
  # def edit
  #   super
  # end

  # PUT /resource/password
  # def update
  #   super
  # end

  protected

  # def after_resetting_password_path_for(resource)
  #   super(resource)
  # end

  # The path used after sending reset password instructions
  # def after_sending_reset_password_instructions_path_for(resource_name)
  #   super(resource_name)
  # end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u|
      u.permit(:email, :password, :password_confirmation, :member_attributes => [:title, :firstname, :lastname, :grade])
    }
  end


end



