class PaymentsController < ApplicationController



  before_filter :authenticate_user!
  before_action :set_payment, only: [:show, :edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token, :only => [:create, :new, :change_payment]

  # GET /payments
  # GET /payments.json
  def index
    @user = current_user
    @payments = @user.payments
    @payment = @payments.last
    if current_user.payments.last.nil?
      redirect_to new_payment_path
    
    else
      unless current_user.payments.last.payment_customer_id.nil?
        @payment_info = Braintree::Customer.find(current_user.payments.last.payment_customer_id)
        @subscription = @payment_info.payment_methods.map(&:subscriptions).flatten.last
        token = @subscription.payment_method_token
        @payment_method = Braintree::PaymentMethod.find(token)
      
        @transactions = Braintree::Transaction.search do |search|
          search.customer_id.is @payment_info.id
        end

        @sub
        
        
      end   
    end
  end

  # GET /payments/1
  # GET /payments/1.json
  def show
    render :show, notice: "hello"
  end

  # GET /payments/new
  def new
    payment_customer = Braintree::Customer.create(email: current_user.email)
    
    @token = Braintree::ClientToken.generate(
      customer_id: payment_customer.customer.id
    )

    @payment = Payment.new
  end

  def edit
    @token = Braintree::ClientToken.generate(
      customer_id: current_user.payments.last.payment_customer_id
    )
  end

  def create
    sub = Braintree::Subscription.create(
        payment_method_nonce: params[:payment_method_nonce],
        plan_id: "efdma-membership"
      )
      if sub.success?

      @payment = Payment.new(payment_customer_id: sub.subscription.transactions.first.customer_details.id, next_payment: Date.today + 1.year, user_id: current_user.id)
      

      respond_to do |format|
        if @payment.save
          UserMailer.confirmation_payment(current_user).deliver
          UserMailer.member_paid(current_user, sub.subscription.transactions.first.payment_instrument_type).deliver
          if current_user.member.member_type == 0 
            current_user.member.update(member_type: 1) 
            NewMember.create(user_id: current_user.id)  
          end
          
          format.html { redirect_to edit_member_path(current_user.member), notice: 'Payment was successfully created. Please upload a photo and add an address to your profil.' }
          format.json { render :show, status: :created, location: @payment }
        else
          format.html { render :new }
          format.json { render json: @payment.errors, status: :unprocessable_entity }
        end
      end
      else
        errors = ""
        sub.errors.each do |error|
          errors += error.message
        end
        redirect_to new_payment_path, notice: errors
      end

    
  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update

  end



  def change_payment
    @customer = Braintree::Customer.find(current_user.payments.last.payment_customer_id)

    subscription_id = @customer.payment_methods.map(&:subscriptions).flatten.first.id
    result = Braintree::Subscription.update(subscription_id, :payment_method_nonce => params[:payment_method_nonce])
    Rails.logger.debug "NEW BRAINTREE PAYMENT: #{result.inspect}"
    if result.success?
      UserMailer.new_payment_method(current_user.id).deliver_now
      redirect_to payments_path, notice: "payment method updated"
    else 
      render :change_payment
    end
    
  end

  def sofort_payment
    

    user = User.find(current_user.id)

    p = Payment.new(user_id: user.id, next_payment: Date.today + 1.year)
    
    respond_to do |format|
      
        if p.save
          UserMailer.confirmation_payment(user).deliver
          UserMailer.member_paid(user, "sofort-ueberweisung").deliver

          if current_user.member.member_type == 0 
              current_user.member.update(member_type: 1) 
              NewMember.create(user_id: current_user.id)  
          end

          format.html { redirect_to edit_member_path(user.member), notice: 'Payment was successfully created. Please upload a photo and add an address to your profil.' }
        end
      
    end
  end

  def send_sofort_payment
    client = Sofort::Client.new
    @response = client.pay(140, 'EFDMA', { success_url: 'http://member.fdm-europe.com/sofort/receive', abort_url: 'http://member.fdm-europe.com/dashboard', language_code: "de", user_variable: current_user.id, notification_url: "http://member.fdm-europe.com/sofort/receive", reason: 'EFDMA Membership' } )
    redirect_to @response["payment_url"]
  end


  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to payments_url, notice: 'Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def show_invoice
    
    if current_user.member.addresses.where(billing_address: true).first.nil?
      redirect_to edit_member_path(current_user.member), notice: "Please add an address in order to create a payment confirmation. (Please check if you billing address is checked as true)."
    else
      @payment
      @transaction = Braintree::Transaction.find(params[:transaction_id])
      @billing_address = current_user.addresses.where(billing_address: true).first
      

      pdf = Prawn::Document.new page_size: "A4"
      image = Rails.root.join('app','assets','images','Logo-4C.jpg').to_s
      pdf.image image, :width => 250, position: :right
      pdf.pad_top (40) {pdf.text "from\nEFDMA\nLerchenfelder Straße 66-68/1/52\n1080 Vienna\nAustria", align: :left, size: 12 }
          
      text_block  = "to\n" + current_user.member.full_name
      text_block += "\n"
      text_block += @billing_address.street + "\n" + @billing_address.plz + " " + @billing_address.city + "\n" +  @billing_address.country+ "\n"

      pdf.pad_top (40) {pdf.text text_block, align: :left, size: 12 }
      pdf.pad_top (40) {pdf.text "Vienna, #{@transaction.created_at.strftime("%Y-%m-%d")}", align: :right, size: 12 }
      pdf.pad_top (10) {pdf.text "Payment confirmation (ID: #{@transaction.id})", align: :left, size: 12, style: :bold }
      pdf.pad_top (20) {pdf.text "Dear #{current_user.member.title} #{current_user.member.firstname} #{current_user.member.lastname} #{current_user.member.grade},\n this is a confirmation of your payment for the EFDMA membership fee for the next year.", align: :left, size: 12}
      pdf.pad_top (20) {pdf.text "We successfully received your payment of € #{@transaction.amount}.", align: :left, size: 12, style: :bold}
      pdf.pad_top (20) {pdf.text "Thank you for being a part of EFDMA.", align: :left, size: 12}
      pdf.pad_top (40) {pdf.text "Yours sincerely,\n\n Lars Werner\nTreasurer EFDMA", align: :left, size: 12}

      pdf.bounding_box([0,50], width: 150, height: 50) do
        pdf.text "EFDMA\nLerchenfelder Straße 66-68/1/52\nA-1080 Vienna", size: 10
      end
      pdf.bounding_box([190,50], width: 150, height: 50) do
        pdf.text "www.fdm-europe.com\noffice@fdm-europe.com\nZVR 898629488", size: 10, align: :center
      end
      pdf.bounding_box([370,50], width: 150, height: 50) do
        pdf.text "IBAN: AT401200050030001405\nBIC:BKAUATWW\nBank Austria - Creditanstalt", size: 10, align: :right
      end

      send_data pdf.render, filename: "invoice_EFDMA_#{current_user.id}.pdf", type: "application/pdf"
    end
  end 






 def show_invoice_sofort

      @user = current_user
      @payment = Payment.find(params[:id])
    
    if current_user.member.addresses.where(billing_address: true).first.nil?
      redirect_to edit_member_path(current_user.member), notice: "Please add an address in order to create a payment confirmation. (Please check if you billing address is checked as true)."
    else
      
      @billing_address = current_user.addresses.where(billing_address: true).first

      pdf = Prawn::Document.new page_size: "A4"
      image = Rails.root.join('app','assets','images','Logo-4C.jpg').to_s
      pdf.image image, :width => 250, position: :right
      pdf.pad_top (40) {pdf.text "from\nEFDMA\nLerchenfelder Straße 66-68/1/52\n1080 Vienna\nAustria", align: :left, size: 12 }
          
      text_block  = "to\n" + current_user.member.full_name
      text_block += "\n"
      text_block += @billing_address.street + "\n" + @billing_address.plz + " " + @billing_address.city + "\n" +  @billing_address.country+ "\n"

      pdf.pad_top (40) {pdf.text text_block, align: :left, size: 12 }
      pdf.pad_top (40) {pdf.text "Vienna, #{@payment.created_at.strftime("%Y-%m-%d")}", align: :right, size: 12 }
      pdf.pad_top (20) {pdf.text "Dear #{current_user.member.title} #{current_user.member.firstname} #{current_user.member.lastname} #{current_user.member.grade},\n this is a confirmation of your payment for the EFDMA membership fee for the next year starting today (#{@payment.created_at.strftime("%Y-%m-%d")}).", align: :left, size: 12}
      pdf.pad_top (20) {pdf.text "We successfully received your payment of € 140,-.", align: :left, size: 12, style: :bold}
      pdf.pad_top (20) {pdf.text "Thank you for being a part of EFDMA.", align: :left, size: 12}
      pdf.pad_top (40) {pdf.text "Yours sincerely,\n\n Lars Werner\nTreasurer EFDMA", align: :left, size: 12}

      pdf.bounding_box([0,50], width: 150, height: 50) do
        
        pdf.text "EFDMA\nLerchenfelder Straße 66-68/1/52\nA-1080 Vienna", size: 10
      end
      pdf.bounding_box([190,50], width: 150, height: 50) do
        
        pdf.text "www.fdm-europe.com\noffice@fdm-europe.com\nZVR 898629488", size: 10, align: :center
      end
      pdf.bounding_box([370,50], width: 150, height: 50) do
        
        pdf.text "IBAN: AT401200050030001405\nBIC:BKAUATWW\nBank Austria - Creditanstalt", size: 10, align: :right
      end

      send_data pdf.render, filename: "invoice_EFDMA_#{@user.id}.pdf", type: "application/pdf"
  end
end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_params
      params.require(:payment).permit(:user_id, :payment_customer_id, :next_payment)
    end
end
