class CertificatesController < ApplicationController
  
  before_filter :authenticate_user!
  #before_filter :not_paid
  require "prawn"

  def show
    @certificate = Certificate.find(params[:id])
    @course = @certificate.course
    pdf = Prawn::Document.new page_size: "A4"
      pdf.canvas do 
        [595.28, 841.89]
        bg_image = Rails.root.join('app','assets','images','EFDMA_BC_background.jpg').to_s
        pdf.image(bg_image)

      end
      
      pdf.bounding_box([0, 580], :width => 523, :height => 100) do
      end



    text_block = "has successfully completed FDM training with at least 60 units.\n on #{@course.begin.strftime("%Y-%m-%d")} in #{@course.city} with the"
    

    pdf.pad_top(50) {pdf.text @certificate.name, align: :center, size: 40, style: :bold, color: "000000"}
    pdf.pad_top (20) {pdf.text text_block, align: :center, size: 14 }
    pdf.pad_top (20) {pdf.text "FDM Basic Certificate (FDM BC)", align: :center, size: 18 }
    pdf.pad_top (20) {pdf.text "The training is in accordance with the Curriculum
of the European Fascial Distortion Model Association (EFDMA)", align: :center, size: 14 }

    

    

    
    
    pdf.pad_top(30) {pdf.text "#{@course.city}, #{@course.end.strftime("%Y-%m-%d")}", align: :center} 
    pdf.pad_top(10) {pdf.text "ID: #{@certificate.special_id}", align: :center}

    pdf.bounding_box([0, 30], width: 200, height: 100) do
      pdf.text @course.instructor_name , align: :center
    end
    pdf.bounding_box([320, 30], width: 200, height: 100) do
      pdf.text @course.organizer_name, align: :center
    end
    unless @course.organizer.avatar.url.nil?
        logo =  open("http://member.fdm-europe.com" + @course.organizer.avatar.url ) 

        pdf.image logo, :at => [350,140], :width => 120 

    end
    send_data pdf.render, filename: "Certificate.pdf", type: "application/pdf"


  end

  def index
	  @certificates = Certificate.where(member_id: current_user.member.id)

  end

  def new
  end

  def create
    @course = Course.find(params["certificate"]["course_id"].to_i)
    @names = params["certificate"]["name"]
    @names_array = @names.split("\r\n")
    pdf = Prawn::Document.new page_size: "A4"
    
    
    

    pdf.font_families.update(
      "DejaVuSans" => {
        normal: Rails.root.join('app', 'assets', 'fonts', 'DejaVuSans.ttf').to_s,
        bold: Rails.root.join('app', 'assets', 'fonts', 'DejaVuSans-Bold.ttf').to_s 
        
      }
    )
    
    pdf.font "DejaVuSans"





    

    
    for item in @names_array
      certificate = Certificate.new(name: item)
      certificate.course_id = @course.id
      certificate.instructor_id = @course.instructor_id
      certificate.organizer_id = @course.organizer_id
      certificate.save


              pdf.canvas do 
          [595.28, 841.89]
          bg_image = Rails.root.join('app','assets','images','EFDMA_BC_background.jpg').to_s
          pdf.image(bg_image)

        end
      
        pdf.bounding_box([0, 580], :width => 523, :height => 100) do
        end



        text_block = "has successfully completed FDM training with at least 60 units.\n on #{@course.begin.strftime("%Y-%m-%d")} in #{@course.city} with the"
    

        pdf.pad_top(50) {pdf.text item, align: :center, size: 40, style: :bold, color: "000000"}
        pdf.pad_top (20) {pdf.text text_block, align: :center, size: 14 }
        pdf.pad_top (20) {pdf.text "FDM Basic Certificate (FDM BC)", align: :center, size: 18 }
        pdf.pad_top (20) {pdf.text "The training is in accordance with the Curriculum
    of the European Fascial Distortion Model Association (EFDMA)", align: :center, size: 14 }

        pdf.pad_top(30) {pdf.text "#{@course.city}, #{@course.end.strftime("%Y-%m-%d")}", align: :center} 
        pdf.pad_top(10) {pdf.text "ID: #{certificate.special_id}", align: :center}

        pdf.bounding_box([0, 30], width: 200, height: 100) do
          pdf.text @course.instructor_name , align: :center
        end
        unless @course.organizer.avatar.url.nil?
        logo =  open("http://efdma.dev" + @course.organizer.avatar.url ) 

        pdf.image logo, :at => [360,150], :width => 100

        end
        
        pdf.bounding_box([320, 30], width: 200, height: 100) do
          pdf.text @course.organizer_name, align: :center
        end


        unless item.equal? @names_array.last
          pdf.start_new_page
        end





      
    end

    send_data pdf.render, filename: "certificates_#{@course.city}_#{@course.begin.strftime("%Y_%m_%d")}.pdf", type: "application/pdf"
  end

  def save_certificate 
    member = current_user.member
    special = params["special_id"]
    certificate = Certificate.where(special_id: special)
    
    if certificate.first.member_id.nil?
      certificate.first.member_id = member.id
      certificate.first.save
      
      current_user.member.update(certified: true)
      
      redirect_to certificates_path, notice: "Certificate was added to your profile"
    else
      redirect_to certificates_path, alert: "Certificate ID is taken by somebody else. Please check if your information is correct. "
    end

    

  end
  
  private


  def not_paid
      unless current_user.member.member_type == 4 # Members with member_type 4 are administrative Admins
        if current_user.payment.nil? && current_user.member.member_type > 0
        redirect_to new_payment_path
      elsif current_user.member.member_type == 0 && current_user.member.trial_ends < Time.now
        redirect_to new_payment_path
      end
      end
  end


end
