class MembersController < ApplicationController
  respond_to :json, :html
  before_filter :authenticate_user!, except: [:show_members, :show, :index, :lastnames, :search_by_name, :save_congress_member,]
  #before_filter :not_paid, except: [:show_members, :show, :index, :lastnames, :search_by_name]
  protect_from_forgery :except => :save_congress_member





  def index
	  @members= Member.where("lastname > ''") 
	  
	  
	  
	  
	  respond_to do |format|
      	format.html 
	  	  format.json { render json: @members, include: [:certificates, :addresses], content_type: "application/javascript", callback: params["callback"] }
      end
  end

  def lastnames
    lastnames = []
    for person in Member.all.order("lastname").where("member_type < 3")
      if person.member_type == 0 && person.trial_ends.to_date < Date.today
      elsif person.user.payments.count > 0 
        if person.user.payments.last.next_payment.to_date < Date.today
          unless person.user.payments.last.payment_customer_id.nil?
            lastnames.push(person.lastname)
          end
        else
          lastnames.push(person.lastname)
        end
      else  
        lastnames.push(person.lastname)
      end
    end
    @lastnames = lastnames.uniq.to_a
    
    respond_to do |format|
      format.json { render json: @lastnames, content_type: "application/json" }
    end

  end

  def search_by_name
    #@searched_names = Member.where("lastname LIKE ?", params[:name]).where("member_type < 3").to_a
    @searched_names = Member.search_by_full_name(params[:name]).where("member_type < 3").to_a
    Rails.logger.debug "#{@search_by_name.to_s}"
     i = 0

     while i <= @searched_names.length
      if @searched_names[i].nil?
        i += 1
      else

         user = User.find(@searched_names[i].user_id)
           payments = user.payments
           if payments.last.nil? && user.member.member_type > 0 #get rid of everybody who hasn't paid yet
             p "DELETED: #{user.id} - not paid"
             @searched_names.delete_at(i) 

           elsif user.member.member_type == 0 && user.member.trial_ends.to_date < Date.today #get rid of everybody whose trail is over
             p "DELETED: #{user.id} - trail over"
             @searched_names.delete_at(i) 
           
           else

              unless payments.empty?

                if payments.last.next_payment.to_date < Date.today && user.member.member_type > 0 && payments.last.payment_customer_id.nil?
                    
                      p "DELETED: #{user.id} - not paid"
                      @searched_names.delete_at(i) 
                    

                else
                  i += 1
                end
              
              else
                  i += 1
              end

              
           end
        end
        
      end


    respond_to do |format|
      format.json { render json: @searched_names, content_type: "application/json" }
    end
  end
  
  
  def new
	  @member = Member.new
  end
  
  def create
	  member = Member.create(member_params)
	  UserMailer.efdma_notification_new_member(user).deliver
	  redirect_to member
  end
  
  def show
  	@member = Member.find(params[:id])
    
    respond_to do |format|
      format.html 
      format.json { render json: @member, include: [:addresses], content_type: "application/javascript", callback: params["callback"] }
      end
  end
  def dashboard
    if current_user.payments.last.nil? && current_user.member.member_type > 0
      redirect_to new_payment_path
    elsif current_user.member.trial_ends.to_date < Date.today && current_user.member.member_type == 0
      redirect_to new_payment_path
    else
      @courses = Course.where("begin >= ?",  Date.today).order("begin").limit(10)
      @new_member_count = User.where("created_at > ?", 60.days.ago).count
      @total_member_count = User.all.count

      begin
        feed = open("http://www.fdm-europe.com/en_US/custom-feed-do-not-change/")

        efdma_news_feed = Nokogiri::XML(feed)



      blogposts_raw = efdma_news_feed.css("item").take(10)
       @blogposts = []
      
      blogposts_raw.each_with_index do |post, i|
        
         @blogposts[i] = {}
         @blogposts[i]["link"]  = post.css("link").text
         @blogposts[i]["title"]  = post.css("title").text
         @blogposts[i]["salty"] = post.css("salty").text
         @blogposts[i]["published"] = post.css("pubDate").text

        

        

        
       end
      rescue OpenURI::HTTPError => error
        @blogposts = []
      end

      
    end

    
  end

  def show_members	  
	  @members = Member.where(member_params).where(member_type < 3)
  end

  def add_certificate
  end

  def save_certificate
  	member = Member.find(params[:id])
  	special = params["special_id"]
  	certificate = Certificate.where(special_id: special)
  	
  	certificate.first.member_id = member.id
  	certificate.first.save

  	redirect_to member, notice: "Zertificate added to your profile"


  end

  def edit
    
    @member = current_user.member
  end
  def update
    
    @member = current_user.member
    @member.update(member_params)
    respond_to do |format|
      format.html { redirect_to edit_member_path(@member) }
    end
    
  end
  
  def contact
    
  end

  def send_contact
    user = User.where(email: params['email']).first
    RestClient.post("https://api:key-45ypl5q56gmanvcb0e3yxriy2o0ci0w6@api.mailgun.net/v3/fdm-europe.com/messages",
      :from => "#{user.member.full_name} <#{params['email']}>",
      :to => "office@fdm-europe.com",
      :subject => "Anfrage von Member Area",
      :html => params["message"])
    
    redirect_to root_url, notice: "Your message has been sent. Thank you!"
  end

  def save_congress_member
    email = params[:email]
    firstname = params[:firstname]
    lastname = params[:lastname]


    @user = User.where(email: email).first_or_create
    @user.update(password: "FDM-CONGRESS-WELCOME")
    @member = Member.create(user_id: @user.id, firstname: firstname, lastname: lastname, member_type: 1)
    Payment.create(next_payment: Date.today + 365.days, user_id: @user.id)
    NewMember.create(user_id: @user.id)  
    
    respond_with @user

  end


end




private


  def not_paid
    unless current_user.member.member_type == 4 # Members with member_type 4 are administrative Admins
      if current_user.payment.nil? && current_user.member.member_type > 0
        redirect_to new_payment_path
      elsif current_user.member.member_type == 0 && current_user.member.trial_ends < Time.now
        redirect_to new_payment_path
      end
    end
  end

	def member_params
      params.require(:member).permit(:title, :firstname, :lastname, :grade, :callback, :id, :member_id, :avatar, :trial_ends, :certified, :organizer_name, :birthday, :job, :member_type)
      
    end