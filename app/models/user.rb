class User < ActiveRecord::Base
	require "rolify"
	
	#Paymill.api_key = "c1cb231f003f2959c2b110abec983263"
	rolify
 	
	

 	has_many :payments, :dependent => :delete_all
 	has_one :member, :dependent => :delete
	has_many :certificates, through: :member, :dependent => :delete_all
	has_many :addresses, through: :member, :dependent => :delete_all
	has_one :avatar, through: :member, :dependent => :delete
  has_one :NewMember
  has_many :coupon
	accepts_nested_attributes_for :member

  after_save :update_mailing_list, if: :email_changed?
  


	after_create :add_default_role, :create_coupon

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  before_destroy :no_delete


    def add_default_role
    	add_role(:member)

      UserMailer.welcome_member(self).deliver


    end

    def delete_from_mailing_list
      DeleteFromMailingListJob.perform_later(self)
    end

    def no_delete
      
      return false
    end

    def create_coupon
      Coupon.create(user_id: self.id)
    end

    def has_paid?
    	if self.payment.next_payment > Date.today
    		return true
    	else
    		return false
    	end
    end

    def has_active_trial?
      if self.member.trial_ends > Time.now
        return true
      else
        return false
      end
    end

    def update_mailing_list
      if self.new_record?
        SubscribeToMailingListJob.perform_later(self)
      else
        UpdateUserInMailingListJob.perform_later(self)
      end
    end

    def self.to_csv


    CSV.generate do |csv|
      csv << column_names
      all.each do |product|
        csv << product.member.attributes.values
      end
    end
  end

end
