	class Course < ActiveRecord::Base
	resourcify
	has_many :certificates
	geocoded_by :course_location
	before_validation :geocode


	validates :instructor_id, :organizer_id, :begin, :end, :city, :course_type, :link, :presence => true
	validate :check_date, :has_location?

	before_save :check_link
	after_create :update_location

	belongs_to :instructor, class_name: "Member", foreign_key: "instructor_id", :primary_key => "user_id"
	belongs_to :organizer, class_name: "Member", foreign_key: "organizer_id", :primary_key => "user_id"



	def instructor_name
		return User.find(instructor_id).member.full_name
	end
	def organizer_name
		return User.find(organizer_id).member.organizer_name
	end
	def duration
		
		start = self.begin
		over = self.end

		duration = (over.to_date - start.to_date + 1).to_i
		

			if duration > 1
				return duration.to_s + " days"
			else
				duration = duration 
				return duration.to_s + " day"
			end

		
		
	end

	def course_location
		return self.city
	end

	def has_location?
		if self.latitude.nil?
			errors.add(:city, "was not found. Is it spelled correctly?")
		end
	end

	def update_location
		Rails.logger.debug("#{self.id}")
		result = Geocoder.search("#{self.latitude},#{self.longitude}")

		
		unless result.nil?
			city = ""
			country = ""

			unless result.first.nil?

				for component in result.first.address_components
					if component["types"].include?("locality")
						city = component["long_name"]
					elsif component["types"].include?("country")
						country = component["long_name"]
					end
				end

				self.city = "#{city}, #{country}"
				Rails.logger.debug("#{self.city}")
				self.save
			end
		end
	end

	def check_link
		course = self
		if course.link.include?("@")
			unless course.link.include?("mailto:")
				email = course.link
				course.update(link: "mailto:" + email)
			end
		else
			unless course.link.include?("http://")
				link = course.link
				course.update(link: "http://" + link)
			end
		end
	end

	def check_date
		begin_date = self.begin 
		end_date = self.end

		if end_date < begin_date
			errors.add(:end, "date cannot be before start date")
	    end
	end

	def send_email_to_instructor_on_last_day_of_course
		
		Course.all.each do |course|
			if course.end.to_date == Date.today
				instructor = User.find(course.instructor_id)
				text = "<p>Liebe/r #{course.instructor_name},</p>
 
<p>wir hoffen dein FDM-Kurs von #{course.begin.strftime("%Y-%m-%d")} bis #{course.end.strftime("%Y-%m-%d")} in #{course.city} war erfolgreich. Bitte sende die Interessentenliste für die EFDMA Mitgliedschaft in den nächsten 2 Wochen an das EFDMA Sekretariat per Email oder Post.</p>
 
<p>Viel Erfolg für deinen nächsten Kurs.</p>
 
<p>Liebe Grüße Martina, <br>im Namen des EFDMA Vorstandes.</p>"
				response = RestClient.post("https://api:key-45ypl5q56gmanvcb0e3yxriy2o0ci0w6@api.mailgun.net/v3/fdm-europe.com/messages", :from => "Office EFDMA <office@fdm-europe.com>", :to => instructor.email, :subject => "It's the last day of the course", :html => text)
				p Time.now
			end
		end 
	end


	def send_email_to_instructor_on_first_day_of_course

				Course.all.each do |course|
			if course.begin.to_date == Date.today
				instructor = User.find(course.instructor_id)
				text = "<p>Liebe/r #{course.instructor_name},</p>
 <p>Bitte vergiss nicht die Interessentenliste für die EFDMA Mitgliedschaft in deinem FDM-Kurs von #{course.start.strftime("%Y-%m-%d")} bis #{course.end.strftime("%Y-%m-%d")}. aufzulegen und anschließend (in den nächsten 2 Wochen) an das EFDMA Sekretariat per Email oder Post zu senden.
<p>Wir wünschen dir Viel Erfolg für deinen Kurs.</p>
<p>Liebe Grüße Martina, <br>
im Namen des EFDMA Vorstandes.</p>"

				response = RestClient.post("https://api:key-45ypl5q56gmanvcb0e3yxriy2o0ci0w6@api.mailgun.net/v3/fdm-europe.com/messages", :from => "Office EFDMA <office@fdm-europe.com>", :to => "design.rupp@gmail.com", :subject => "It's the first day of the course", :html => text)
				p Time.now
			end
		end 

	end

end
