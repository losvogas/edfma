class Coupon < ActiveRecord::Base

before_create :generate_code
belongs_to :user



def generate_code
    self.code = loop do
      random_code = SecureRandom.urlsafe_base64(nil, false)
      break random_code unless Coupon.exists?(code: random_code)
    end
  end
end
