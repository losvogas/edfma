class Payment < ActiveRecord::Base
	belongs_to :user
	has_one :member, :through => :user

	validate :one_payment_per_year, on: "create"

	def one_payment_per_year
		if Payment.where(user_id: self.user_id, next_payment: self.next_payment).count > 0
			errors.add(:next_payment, "There is already a payment with the same next payment date.")
		end
	end

	
end
