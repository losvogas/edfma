class Address < ActiveRecord::Base
	resourcify
	belongs_to :member
	has_one :payment, through: :user
	geocoded_by :full_street_address
	after_validation :geocode

	validates :street, :city, :plz, :country, presence: true

	after_save :only_one_primary_and_billing_address
	


	




	def full_street_address
		return "#{street}, #{plz} #{city}"
	end


	def only_one_primary_and_billing_address
		member = Member.find(self.member_id)
		if member.addresses.count > 1
			if self.billing_address == true
				Address.where("member_id = #{member_id} AND id != #{self.id}").update_all(billing_address: false)
				
			end


			if self.primary == true
				Address.where("member_id = #{member_id} AND id != #{self.id}").update_all(primary: false)
				
			end
		end

		

		

		
	end








end
