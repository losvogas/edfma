class Scholar < ActiveRecord::Base
  include PgSearch
  pg_search_scope :search_for, against: %i(title content author)

	belongs_to :scholar_category
	mount_uploader :document, DocumentUploader
	has_one :document

  validates :title, :author, :scholar_category_id, presence: true

	before_create :generate_code
  after_create :generate_content

	def generate_code
	    self.link = loop do
	      random_code = SecureRandom.urlsafe_base64(nil, false)
	      break random_code unless Scholar.exists?(link: random_code)
	    end

  	end

  	def external_link
  		return "http://efdma.dev" + "/scholars/external/" + self.link
  	end

  	def category
  		return ScholarCategory.find(self.scholar_category).name
  	end

  	def mother_category
  		return ScholarCategory.find(self.scholar_category).mother_category.name
  	end

    def self.search_category(category_id)
      sc = ScholarCategory.find(category_id)
      list = ScholarCategory.where("custom_id like ?", sc.custom_id +  "%").map(&:id)
      self.where(scholar_category_id: list)
    end

    def self.author_names
      self.all.map(&:author).uniq
    end



    def generate_content
      reader = PDF::Reader.new(self.document.file.file)
      content = ""
      reader.pages.each do |page|
        content += page.text
      end

      self.update(content: content)

    end

end
