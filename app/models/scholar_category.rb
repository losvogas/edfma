class ScholarCategory < ActiveRecord::Base
	has_many :scholars

	has_many :subcategories, :class_name => "ScholarCategory", :foreign_key => "parent_id", :dependent => :destroy
  	belongs_to :parent_category, :class_name => "ScholarCategory"


	def self.all_mother_categories
		return ScholarCategory.where(parent_id: nil)
	end

	def self.all_children_categories
		return ScholarCategory.where.not(parent_id: nil)
	end

	def mother_category
		ScholarCategory.find(self.parent_id)
	end

	def get_scholars_in_category
		listed_scholars = []

		self.scholars.each do |scholar|
			listed_scholars.push(scholar)
		end

		for subs in self.subcategories
			

			subs.scholars.each do |scholar|
				listed_scholars.push(scholar)
			end

			if subs.subcategories.count > 0 
				get_sub_scholars(subs.subcategories)
			end

		end

		return listed_scholars

	end



	def get_sub_scholars(subcategories)
		for subs in subcategories
			subs.scholars.each do |scholar|
				listed_scholars.push(scholar)
			end
			
			if subs.subcategories.count > 0
				get_sub_scholars(subs.subcategories)
			end
		end
	end



end
