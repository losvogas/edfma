class Certificate < ActiveRecord::Base
	resourcify
	belongs_to :member
	belongs_to :course
	
	
	validates :special_id, uniqueness: true
	
	before_create :create_special_id



	private

	def create_special_id
    	self.special_id = loop do
      	random_token = rand(36**8).to_s(36)
      	break random_token unless self.class.exists?(special_id: random_token)
    end
  end
end
