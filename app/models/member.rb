class Member < ActiveRecord::Base
	resourcify
	belongs_to :user
	has_many :certificates
	has_many :addresses , -> { order(primary: :desc) }
	has_one :avatars
	has_one :payment, :through => :user
	before_create :set_trail_ends
	before_save :check_names

	before_destroy :delete_from_mailing_list

	mount_uploader :avatar, AvatarUploader

	def full_name
		return "#{title} #{firstname} #{lastname} #{grade}"
	end

	def delete_from_mailing_list
      if 1 == 1
        errors[:base] << "cannot delete submission that has already been paid"
      return false
      end
    end

	def print_member_type
		if member_type == 0
			return "Probemitglied"
		elsif member_type == 1
			return "Basic"
		else
			return "International"
		end
	end

	def self.search_by_full_name(query)
  		where("CONCAT_WS(' ', title, firstname, lastname, grade) ILIKE :q", :q => "%#{query}%")
	end 


	

	def set_trail_ends
		self.trial_ends = Time.now + 120.days
	end

	def check_names
		if self.organizer_name.nil?
			self.firstname = self.firstname.chomp(" ")
			self.lastname = self.lastname.chomp(" ")
		end
	end


	def erinnerungs_emails_in_probe
		Member.where(member_type: 0).each do |member|
			if member.trial_ends.to_date - Date.today == 7

				text = "<p>Dear #{member.full_name},</p>
 						<p>Your trial membership ends in 7 days.</p>
						<p>If you are interested in becoming a full member (the membership costs just €140,- per year), please <a href='http://member.fdm-europe.com/payments/new'>click here to learn more about the benefits of our membership</a>.</p>
						<p>Best regards, <br>Martina Lichtblau, general secretary</p>"

				response = RestClient.post("https://api:key-45ypl5q56gmanvcb0e3yxriy2o0ci0w6@api.mailgun.net/v3/fdm-europe.com/messages", :from => "Office EFDMA <office@fdm-europe.com>", :to => member.user.email, :subject => "Your EFDMA trail ends in 7 days", :html => text)
				p response

			elsif member.trial_ends.to_date - Date.today == 2

				text = "<p>Dear #{member.full_name},</p>
 
						<p>Your trial membership ends in 2 days.</p>
						<p>After the trial is over, you lose:</p>

						<ul>
						<li>Possibility to update your profile</li>
						<li>Templates for various forms (e.g. patient information folder)</li>
						<li>Information about studies regarding the FDM and associated topics</li>
						<li>Information about discounts</li>
						<li>Teaching contents (e.g. webinars, congress contributions, etc.)</li>
						<li>News for members</li>
						</ul>
						 
						<p>If you are interested in becoming a full member (the membership costs just €140,- per year), please <a href='http://member.fdm-europe.com/payments/new'>click here to learn more about the benefits of our membership</a>.</p>
						<p>Best regards, <br>Martina Lichtblau, general secretary</p>"

				response = RestClient.post("https://api:key-45ypl5q56gmanvcb0e3yxriy2o0ci0w6@api.mailgun.net/v3/fdm-europe.com/messages", :from => "Office EFDMA <office@fdm-europe.com>", :to => member.user.email, :subject => "Your EFDMA trail ends in 2 days", :html => text)
				p response

			elsif member.trial_ends.to_date - Date.today == 1

				text = "<p>Dear #{member.full_name},</p>
				 
						<p>Your trial membership ends tomorrow.</p>
						<p>Please let me know if you have questions about the membership and the reasons why you should join.</p>
						<p>If you are interested in becoming a full member (the membership costs just €140,- per year), please <a href='http://member.fdm-europe.com/payments/new'>click here to learn more about the benefits of our membership</a>.</p>
						<p>Best regards, <br>Martina Lichtblau, general secretary</p>"

				response = RestClient.post("https://api:key-45ypl5q56gmanvcb0e3yxriy2o0ci0w6@api.mailgun.net/v3/fdm-europe.com/messages", :from => "Office EFDMA <office@fdm-europe.com>", :to => member.user.email, :subject => "Last day to become a EFDMA member", :html => text)
				p response

			elsif member.trial_ends.to_date - Date.today == 0
				text = "<p>Dear #{member.full_name},</p>
 
						<p>Your trial membership ends today.</p>
						<p>We are very sorry that you couldn't see the value in becoming a member.</p>
						<p>You will be removed from our website (fdm-europe.com) by the end of today.</p>
						<p>In case you change your mind, you can reactivate your membership by becoming a member at any time. Please <a href='http://member.fdm-europe.com/payments/new'>click here to become a full member</a>.</p>
						<p>Best regards, <br>Martina Lichtblau, general secretary</p>"

				response = RestClient.post("https://api:key-45ypl5q56gmanvcb0e3yxriy2o0ci0w6@api.mailgun.net/v3/fdm-europe.com/messages", :from => "Office EFDMA <office@fdm-europe.com>", :to => member.user.email, :subject => "Your EFDMA trial has ended", :html => text)
				p response
			end


		end
	end

	def send_email_with_invoice_link
		Member.where("member_type > 0").each do |member|
			unless member.user.nil?
				if member.user.payments.count > 0
					p member.full_name
					unless member.user.payments.last.payment_customer_id.nil?
						p "prepare to send email"
						transactions = Braintree::Transaction.search do |search|
	          				search.customer_id.is member.user.payments.last.payment_customer_id
	        			end

	        			t = transactions.first


	        			if t.created_at.to_date  == Date.today - 1.day 	

	        				text = "<p>Dear #{member.full_name},</p>
	 
							<p>yesterday you paid your membership fee with an automatic payment.</p>
							<p>You can view your invoice on the payments page of the member area. <br> <a href='http://member.fdm-europe.com/payments'>Visit payments page</a>.</p>
							<p>Best regards, <br>Martina Lichtblau, general secretary</p>"

					response = RestClient.post("https://api:key-45ypl5q56gmanvcb0e3yxriy2o0ci0w6@api.mailgun.net/v3/fdm-europe.com/messages", :from => "Office EFDMA <office@fdm-europe.com>", :to => member.user.email, :subject => "Payment notification", :html => text)
					p response

	        			else
	        				p "nothing to do"
	        			end

					end
				end
			end
		end
	end

	def self.search_by_braintree(query)
		Payment.where(payment_customer_id: query).last.user.member
	end
end
