require 'rufus-scheduler'


scheduler = Rufus::Scheduler.new


unless scheduler.down?
	
	scheduler.cron '00 05 * * *' do
		Course.last.send_email_to_instructor_on_last_day_of_course
		Rails.logger.info "Kurserinnerungen wurden verschickt"
	end

	scheduler.cron '00 08 * * *' do
		Member.last.erinnerungs_emails_in_probe
		Rails.logger.info "Probemitgliedsemails wurden verschickt"
	end

	scheduler.cron '00 07 * * *' do
		Course.last.send_email_to_instructor_on_first_day_of_course
		Rails.logger.info "Kurserinnerung am Anfang des Kurses verschickt"
	end

	scheduler.cron "00 01 * * *" do 
		Member.last.send_email_with_invoice_link
		Rails.logger.info "Hey YO!"
	end


end
		
		
	
