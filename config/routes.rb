Rails.application.routes.draw do
  
  resources :scholars
  resources :quit_accounts
  get 'coupon/check_coupon'

  resources :member_files
  resources :payments
  
  get 'admin/stats'
  get "/admin/new_payment_for_member/:user_id" => "admin#new_payment_for_member", as: "new_payment_for_member"
  post "/admin/create_payment_for_member" => "admin#create_payment_for_member"


  get "admin/look_up" => "admin#look_up"
  get "admin/edit_user/:user_id" => "admin#edit_user", as: "admin_edit_user"
  
  patch "admin/change_user/:user_id" => "admin#change_user", as: "admin_change_user"


  post "change_payment" => "payments#change_payment", as: "change"
  post "list_searched_members" => "admin#list_searched_members", as: "list_searched_members"

  resources :courses
  devise_for :users, controllers: { sessions: "users/sessions", registrations: "users/registrations", payments: "users/payments", passwords: "users/passwords" }
  get "/profile" => "members#edit"
  get "/dashboard/:member_id" => "members#dashboard", as: "dashboard"
  resources :members 

  resources :addresses
  resources :users

  resources :certificates
  post "certificate/save_certificate" => "certificates#save_certificate"
  
  get "members/:id/add_certificate" => "members#add_certificate"
  post "members/:id/save_certificate" => "members#save_certificate"
  get "members/show_members" => "members#show_members"
  get "member_search" => "addresses#search"
  get "lastnames" => "members#lastnames", as: "lastnames"
  get "search_by_name" => "members#search_by_name", as: "search_by_name"

  get "/contact" => "members#contact", as: "contact"
  post "/send_contact" => "members#send_contact", as: "send_contact"
  
  post "/show_invoice" => "payments#show_invoice", as: "show_invoice"
  post "/show_invoice_sofort" => "payments#show_invoice_sofort", as: "show_invoice_sofort"
  get "/admin/show_invoice/:user_id" =>"admin#show_invoice", as: "admin_show_invoice"
  get "/edit_overview" => "courses#edit_overview", as: "edit_overview"
  
  post "resend_member_email/:user_id" => "admin#resend_member_email"

  get "/admin/:user_id/addresses/new" => "admin#new_address", as: "admin_new_address"
  post "admin/:user_id/addresses" => "admin#create_address", as: "admin_create_address"
  delete "admin/addresses/:id" => "admin#destroy_address", as: "admin_destroy_address"
  get "admin/addresses/:id" => "admin#edit_address", as: "admin_edit_address"
  patch "admin/addresses/:id" => "admin#update_address", as: "admin_update_address"
  get "admin/has_paid" => "admin#has_paid"
  post "admin/send_payment_reminder" => "admin#send_payment_reminder"
  get "admin/new_user" => "admin#new_user"
  post "admin/create_user" => "admin#create_user", as: "admin_create_user"
  post "admin/user/:id" => "admin#delete_user", as: "admin_delete_user"
  get "admin/organizer/new" => "admin#new_organizer"
  get "admin/organizer/edit/:id" => "admin#edit_organizer", as: "admin_edit_organizer"
  post "admin/organizer" => "admin#create_organizer", as: "admin_create_organizer"
  patch "admin/organizer/:id" => "admin#update_organizer", as: "admin_update_organizer"
  get "admin/organizers" => "admin#index_organizers", as: "admin_organizers"
  post "admin/organizers/delete/:id" => "admin#delete_organizer", as: "admin_delete_organizer"
  get "admin/who_became_member" => "admin#who_became_member", as: "who_became_member"
  get "sofort/receive" => "payments#sofort_payment", as: "sofort_payment"

  post "sofort/success" => "payments#sofort_success", as: "sofort_success"
  get "sofort/sent_sofort" => "payments#send_sofort_payment", as: "send_sofort_payment"

  get "coupon/check" => "coupon#check", defaults: { format: "json"}
  get "coupon/used_now" => "coupon#used_now", defaults: { format: "json"} 
  

  get "save_congress_member" => "members#save_congress_member"
  get "admin/quit_member" => "quit_accounts#index", as: "admin_quit_members"

  post "admin/webhooks" => "admin#braintree_webhooks"

  get "scholars/external/:link" => "scholars#external_link"
  post "scholars/list_searched_scholars" => "scholars#list_searched_scholars"


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'members#dashboard'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
