require "rails"
require "csv"
require "rest-client"

def sorry_list
	@emails = []
	for user in User.all
		if user.payments.count > 1
			sofort = 0
			kk = 0

			for payment in user.payments
				if payment.payment_customer_id.nil?
					sofort += 1
				else
					kk += 1
				end
			end

			if sofort > 0 && kk > 0
				@emails.push(user.email)
			end
		end
	end
end


def print_stuff
	p "hallo, ich lebe"
	
end

def who_has_more
	Payment.where.not(payment_customer_id: nil).each do |payment|
		b = Braintree::Customer.find(payment.payment_customer_id)
		s = b.payment_methods.map(&:subscriptions).flatten

		if s.count > 1
			p b
		end
	end
end

def paymentemail
	for payment in Payment.where("next_payment < ?", "2017-01-02".to_date)
		unless payment.user.nil?
			if payment.payment_customer_id.nil?
				send_reminder_ueberweisung(payment.user) 
			end
		end
	end
end


def send_codes_to_members(user)
	user = user

		html = "<p>Hallo #{user.member.full_name}!</p>
 

<p>Als EFDMA Mitglied erhältst du einen exklusiven Mitgliederrabatt für den 7. FDM World Congress (Anmeldung unter www.fdm-congress.com ). Diesen Rabatt kannst du mit deinem personalisierten Rabatt Code <strong>#{user.coupon.first.code }</strong> einlösen. Bitte gib deinen Code am Ende des Anmeldevorgangs (beim Bezahlformular)  ein, der Rabatt wird dann automatisch abgezogen.</p>
 
<p>Bei Fragen wendet euch bitte an das EFDMA Sekretariat (office@fdm-europe.com , +43 1 94 75 276).</p>
<hr>
 
<p>Dear #{user.member.full_name}!</p>
 
<p>As a member of the EFDMA you receive an exclusive discount code for the 7th FDM World Congress (Registration at www.fdm-congress.com ). You can redeem this discount with your personalized coupon-code <strong>#{user.coupon.first.code }</strong>. Please enter your code  at the end of the registration process (with the payment form). </p>
<p>If you have any questions, please contact the EFDMA office (office@fdm-europe.com, +43 1 94 75 276).</p>

<p>EFDMA<br>
Lerchenfelder Straße 66-68/1/52<br>
A-1080 Wien/Vienna<br>
Tel/Phone.: +43 1 94 75 276<br>
Email: office@fdm-europe.com<br>
www.fdm-europe.com<br>
ZVR: 898629486</p>
"


RestClient.post "https://api:key-45ypl5q56gmanvcb0e3yxriy2o0ci0w6"\
		"@api.mailgun.net/v3/fdm-europe.com/messages",
		:from => "Office EFDMA <office@fdm-europe.com>",
		:to => user.email,
		:subject => "FDM-Congress Coupon-codes.",
		:html => html

end


def send_reminder_ueberweisung(user) 
	user = user

	html = "<p>Sehr geehrte(r) #{user.member.full_name}!</p>
<p>Der Mitgliedsbeitrag 2017 (€ 100) wird am 01.01.2017 fällig. Bitte loggen Sie sich für die Zahlung  in ihren EFDMA Mitglieder Account (http://member.fdm-europe.com) ein und folgen den Zahlungsanweisungen auf ihrem Dashboard. 
Sie erhalten sofort nach erfolgreicher Zahlung einen Link per Email zu ihrer Zahlungsbestätigung, diese können Sie in ihrem EFDMA Mitglieder Account abrufen. Wenn Sie eine jährliche automatische Abbuchung des Mitgliedsbeitrages wünschen, dann wählen Sie bitte die Zahlungsmethode PayPal, Credit card oder Maestro (Debitcard) in ihrem EFDMA Mitglieder Account (http://member.fdm-europe.com) unter dem Menüpunkt „Settings/Payments“ (oben/rechts). Wenn Sie SOFORT Online Bank Transfer (Sofort-Überweisung) wählen, müssen Sie sich jährlich für die Überweisungsaktion in ihren Mitglieder Account einloggen.
Falls der Mitgliedsbeitrag 2017 nicht bis 01.01.17 beglichen wird, wird ihr Profil in der FDM Therapeutenliste automatisch offline geschalten, bis die Zahlung eingeht.
Für Fragen wenden Sie sich bitte an das EFDMA office unter office@fdm-europe.com oder +43 1 94 75 276.</p>
<p>Vielen Dank, dass Sie als Mitglied der EFDMA unsere Aktivitäten, das FDM qualitativ zu sichern und weiter zu verbreiten, unterstützen.</p>
<p>Save the Date: FDM World Congress 2017 in Köln: www.fdm-congress.com (exklusiver Rabatt für EFDMA Mitglieder)</P>

<hr>
<p>Dear #{user.member.full_name}!</p>
<p>The membership fee 2017 (€ 100) is due on January 1st, 2017. For payment please log in your EFDMA member account (http://member.fdm-europe.com), and follow the payment instructions on your dashboard. 
You’ll get an confirmation email after successful payment with the link to your payment confirmation, which you can recall in your EFDMA member account. If you want that the membership fee will be charged automatically from now on, please choose the payment methods PayPal, Credit card or Maestro (Debitcard) in your EFDMA member account(http://member.fdm-europe.com),  under the menue item „Settings/Payments“ (top right). If you choose the payment method SOFORT Online Bank  
Transfer you have to log in your EFDMA member account yearly to do the transfer.</p>
<p>If the membership fee 2017 is not paid until January 1st, 2017, your profile in the FDM practitioners list will turn offline automatically until payment is received.
For questions, please contact the EFDMA office under office@fdm-europe.com or +43 1 94 75 276.</p>
<p>Thank you that, you as a member of the EFDMA, support our activities to secure the FDM qualitatively and spread the FDM further. </p>
<p><strong>Save the Date: FDM World Congress 2017 in Cologne: www.fdm-congress.com (exclusive discount for EFDMA members)</strong></p>

<p>EFDMA<br>
Lerchenfelder Straße 66-68/1/52<br>
A-1080 Wien/Vienna<br>
Tel/Phone.: +43 1 94 75 276<br>
Email: office@fdm-europe.com<br>
www.fdm-europe.com<br>
ZVR: 898629486</p>"
	p html
	RestClient.post "https://api:key-45ypl5q56gmanvcb0e3yxriy2o0ci0w6"\
		"@api.mailgun.net/v3/fdm-europe.com/messages",
		:from => "Office EFDMA <office@fdm-europe.com>",
		:to => user.email,
		:subject => "Your EFDMA membership is due.",
		:html => html

	
end

def send_reminder_creditcard(user)
	user = user

	html = "<p>Sehr geehrte(r) #{user.member.full_name}!</p>
<p>Ihr Mitgliedsbeitrag 2017 (€ 100) wird automatisch am #{user.payment.next_payment.strftime("%d.%m.%Y")} abgebucht  (über die von Ihnen angegebene Zahlungsmethode in ihrem EFDMA Mitglieder Account - gespeichert von der Zahlung des Mitgliedsbeitrages 2016).
Sie erhalten sofort nach erfolgreicher Abbuchung eine Email mit einem Link zu ihrer Zahlungsbestätigung, welche Sie dann in ihrem Mitglieder Account jederzeit abrufen können.
Wenn Sie ihre Zahlungsdetails ändern möchten (z.B. neue Kartendetails, andere Zahlungsmethode) so können Sie dies in ihrem EFDMA Mitglieder Account (http://member.fdm-europe.com),  unter dem Menüpunkt „Settings/Payments“ (oben/rechts) tun.</p>
<p>Für Fragen wenden Sie sich bitte an das EFDMA Sekretariat unter office@fdm-europe.com oder +43 1 94 75 276.</p>
<p>Vielen Dank, dass Sie als Mitglied der EFDMA unsere Aktivitäten, das FDM qualitativ zu sichern und weiter zu verbreiten unterstützen.</p>
<p><strong>Save the Date: FDM World Congress 2017 in Köln: www.fdm-congress.com (exklusiver Rabatt für EFDMA Mitglieder)</strong></p>
<hr>
<p>Dear #{user.member.full_name}!</p>
<p>Your membership fee 2017 (€ 100) will be automatically charged on #{user.payment.next_payment.strftime("%Y.%m.%d")}  (via the payment method you deposited already in your EFDMA member account - saved from the payment of the membership fee 2016).  
You’ll get an confirmation email after successful debiting with the link to your payment confirmation, which you can recall in your EFDMA member account.
If you want to change your payment details (f.ex. new card details, other payment method) you can do this in your EFDMA member account (http://member.fdm-europe.com),  under the menue item „Settings/Payments“ (top right).</p>
<p>For questions, please contact the EFDMA office under office@fdm-europe.com or +43 1 94 75 276. </p>
<p>Thank you that, you as a member of the EFDMA, support our activities to secure the FDM qualitatively and spread the FDM further. </p>
<p><strong>Save the Date: FDM World Congress 2017 in Cologne: www.fdm-congress.com (exclusive discount for EFDMA members)</strong></p>

<p>EFDMA<br>
Lerchenfelder Straße 66-68/1/52<br>
A-1080 Wien/Vienna<br>
Tel/Phone.: +43 1 94 75 276<br>
Email: office@fdm-europe.com<br>
www.fdm-europe.com<br>
ZVR: 898629486</p>"

	 RestClient.post "https://api:key-45ypl5q56gmanvcb0e3yxriy2o0ci0w6"\
	 	"@api.mailgun.net/v3/fdm-europe.com/messages",
		:from => "Office EFDMA <office@fdm-europe.com>",
		:to => user.email,
		:subject => "Your EFDMA membership is due.",
		:html => html

	p html

end







